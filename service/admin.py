from django.contrib import admin
from .models import DeviceRegistry, Device


# Register your models here.
admin.site.register(DeviceRegistry)
admin.site.register(Device)