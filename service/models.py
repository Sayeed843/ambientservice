from django.db import models
from django.utils import timezone
from rest_framework.response import Response
import json



DEVICE_CHOICES = (
    ('actuator', 'actuator'),
    ('sensor', 'sensor'),
)
# Create your models here.
class DeviceRegistryQuerySet(models.query.QuerySet):

    def name_data(self, name):
        return self.filter(name__exact=name)

    def get_metadata(self, metadata):
        return self.filter(metadata__exact=metadata)


class DeviceRegistryManager(models.Manager):

    def get_queryset(self):
        return DeviceRegistryQuerySet(self.model, using=self._db)

    def all_data(self):
        return self.get_queryset().all()

    def get_first(self):
        return self.get_queryset().filter(id=1)

    def get_data(self, query):
        # request = self.request
        qs = self.all_data()

        if query is not None:
            qs = qs.filter(name__exact=query)
        return qs

    def is_valid(self, metadata):

        qs = self.get_queryset().get_metadata(metadata)
        if qs.count() >= 1:
            return False  # If the system find name to the db, then it will get duplicate data after save new data
        return True


class DeviceRegistry(models.Model):
    name = models.CharField(max_length=120)
    metadata = models.CharField(max_length=160)
    timestamp = models.DateTimeField(auto_now_add=True, null=True)

    objects = DeviceRegistryManager()

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        if not self.id:
            self.time = timezone.now()
        return super(DeviceRegistry, self).save(*args, **kwargs)


class DeviceManager(models.Manager):
    def get_last_one(self):
        pass

    def get_device(self, device):
        return self.get_queryset().filter(type__exact=device)

    def get_actuator(self):
        return self.get_queryset().filter(type='actuator')


class Device(models.Model):
    metadata = models.ForeignKey(DeviceRegistry, on_delete=models.CASCADE)
    type = models.CharField(max_length=20, choices=DEVICE_CHOICES, default='actuator')
    data = models.DecimalField(decimal_places=2, max_digits=20, default=0.0)
    timestamp = models.DateTimeField(auto_now_add=True, null=True)

    objects = DeviceManager()

    def __str__(self):
        return self.metadata.name

    def save(self, *args, **kwargs):
        if not self.id:
            self.time = timezone.now()
        return super(Device, self).save(*args, **kwargs)