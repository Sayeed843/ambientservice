from rest_framework import generics, mixins
from service.models import DeviceRegistry, Device
from .serializers import DeviceRegistrySerializer, DeviceSerializer

from rest_framework.response import Response
import json


def is_json(json_data):
    try:
        real_json = json.loads(json_data)
        is_valid = True
    except ValueError:
        is_valid = False
    return is_valid


class ServiceAPIView(mixins.CreateModelMixin, generics.ListAPIView):
    permission_classes = []
    authentication_classes = []
    serializer_class = DeviceRegistrySerializer
    # queryset = Service.objects.all()

    def get_queryset(self):
        request = self.request
        query = request.GET.get('q')
        return DeviceRegistry.objects.get_data(query)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)


class DeviceAPIView(mixins.CreateModelMixin, mixins.RetrieveModelMixin,
                    generics.ListAPIView):
    permission_classes = []
    authentication_classes = []
    serializer_class = DeviceSerializer

    def get_queryset(self):
        request = self.request
        query = request.GET.get('q')
        return Device.objects.get_device(query)

    def get(self, request, *args, **kwargs):
        print("*"*10)
        print(request.body)
        return super().get(request, *args, **kwargs)


