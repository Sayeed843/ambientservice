from django.urls import path
from .views import ServiceAPIView, DeviceAPIView


urlpatterns = [
    path('service/list/', ServiceAPIView.as_view(), name='api-list'),
    path('service/device/', DeviceAPIView.as_view(),),
]