from rest_framework import serializers
from service.models import DeviceRegistry, Device


class DeviceRegistrySerializer(serializers.ModelSerializer):

    class Meta:
        model = DeviceRegistry
        fields = ('pk', 'name', 'metadata')

    def validate(self, data, *args, **kwargs):
        metadata = data.get('metadata', None)

        valid_data = False
        if metadata is not None:
            valid_data = DeviceRegistry.objects.is_valid(metadata)

        if valid_data:
            return data
        raise serializers.ValidationError({"message": "Metadata should be unique!"})


class DeviceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Device
        fields = ('pk', 'metadata', 'type', 'data', 'timestamp')

        def validate(self, data, *args, **kwargs):
            metadata = data.get('metadata', None)
            value = data.get('data', None)

            if metadata is None:
                serializers.ValidationError({"message": "Your device should register to the system."})

            if value is not None:
                pass
            return data